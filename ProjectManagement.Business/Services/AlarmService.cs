﻿using ProjectManagement.Business.Services.Generic;
using ProjectManagement.DataAccess.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Business.Services
{
    public class AlarmService : IGenericCrud<Alarms>
    {
        private static GenericService GS = new GenericService();
        private static LoggerService LS = new LoggerService();

        public int Add()
        {
            try
            {
                return GS.GenericAdd();
            }
            catch (Exception ex)
            {
                LS.Add(ex.Message.ToString());
                return 0;
            }
        }

        public Alarms Get()
        {
            throw new NotImplementedException();
        }

        public List<Alarms> List()
        {
            throw new NotImplementedException();
        }

        public int Remove()
        {
            throw new NotImplementedException();
        }

        public int Update()
        {
            throw new NotImplementedException();
        }
    }
}
