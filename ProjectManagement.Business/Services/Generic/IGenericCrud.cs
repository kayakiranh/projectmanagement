﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Business.Services.Generic
{
    public interface IGenericCrud<T>
    {
        int Add();
        int Update();
        int Remove();
        T Get();
        List<T> List();
    }
}
