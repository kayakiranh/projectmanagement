﻿using ProjectManagement.DataAccess.Datas.Common;
using System;

namespace ProjectManagement.DataAccess.Datas
{
    public class Announcements : UserData
    {
        public string Title { get; set; } = "Title";
        public string Text { get; set; } = "Text";
        public DateTime? Time { get; set; } = DateTime.Now.AddDays(1);
    }
}