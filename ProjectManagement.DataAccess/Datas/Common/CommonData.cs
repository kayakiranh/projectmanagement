﻿using System;

namespace ProjectManagement.DataAccess.Datas.Common
{
    public class CommonData
    {
        public int Id { get; set; }
        public DateTime CreateTime { get; set; } = DateTime.Now;
        public DateTime? UpdateTime { get; set; } = null;
        public bool Status { get; set; } = true;
    }
}