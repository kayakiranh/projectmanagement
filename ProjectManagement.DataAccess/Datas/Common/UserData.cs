﻿namespace ProjectManagement.DataAccess.Datas.Common
{
    public class UserData : CommonData
    {
        public int WhichCompany { get; set; }
        public int WhichUser { get; set; }
    }
}